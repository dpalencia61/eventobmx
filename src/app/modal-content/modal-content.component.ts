import { Component } from '@angular/core';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-modal-content',
  templateUrl: './modal-content.component.html',
  styleUrls: ['./modal-content.component.css']
})

@Injectable({
  providedIn: 'root'
})
export class ModalContentComponent {
  constructor(private dialog: MatDialog) { }

  openSuccessModal(): void {
    this.dialog.open(ModalContentComponent, {
      width: '550px',
      height: '200px',
      
    });
  }
}
