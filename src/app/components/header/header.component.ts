import { Router } from '@angular/router';
import { Component,OnInit } from '@angular/core';
import { BmxDetailComponent } from '../bmx-detail/bmx-detail.component';
import { BmxService } from 'src/app/services/bmx.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit{


  constructor(private router: Router) {}

  ngOnInit(): void {
  
  }

  Subs(){
  this.router.navigate(['/bmxDetail']);
  }

  home(){
    this.router.navigate(['/home']);
  }

  event(){
    this.router.navigate(['/info']);
  }
  shop(){
    alert("Aun no disponible");
  }
}
