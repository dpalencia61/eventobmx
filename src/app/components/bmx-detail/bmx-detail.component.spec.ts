import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BmxDetailComponent } from './bmx-detail.component';

describe('BmxDetailComponent', () => {
  let component: BmxDetailComponent;
  let fixture: ComponentFixture<BmxDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BmxDetailComponent]
    });
    fixture = TestBed.createComponent(BmxDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
