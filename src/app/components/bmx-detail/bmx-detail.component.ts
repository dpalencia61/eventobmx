import { Component, OnInit,} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalContentComponent } from 'src/app/modal-content/modal-content.component';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-bmx-detail',
  templateUrl: './bmx-detail.component.html',
  styleUrls: ['./bmx-detail.component.css']
})
export class BmxDetailComponent{
  registroForm: FormGroup;
  nombre: string = '';
  apellido: string = '';
  email: string = '';
  edad: number | null = null; 
  sexo: string = '';
  categoria: string = '';
  municipio: string = '';
  departamento: string = '';
  fecha: | null = null; 

  constructor(private formBuilder: FormBuilder, 
              private modalContentComponent: ModalContentComponent,
              private dialog: MatDialog)
              
              {

      this.registroForm = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      edad: [null, Validators.required], 
      fecha: ['', Validators.required],
      sexo: ['', Validators.required],
      categoria: ['', Validators.required],
      departamento: ['',Validators.required],
      municipio: ['',Validators.required],
      recaptcha: ['', Validators.required]
    });
  }

  siteKey:string ="6LcKJCMpAAAAAJ3xuxVFPXEbGlk54g04OOwKXy6Y";


  Guardar(): void {

    console.log(this.registroForm.value);

    if (this.registroForm.valid) {
      const formData = this.registroForm.value;
      localStorage.setItem('formularioDatos', JSON.stringify(formData));
      console.log('Datos guardados en localStorage:', formData);
      this.modalContentComponent.openSuccessModal();
    } else {
      console.log('Formulario inválido. No se guardaron datos.');
      alert("Debes llenar todos los campos");


    }
  }
  onChangeNombre(event: any) {
    this.nombre = event.target.value;
  }

  
}
