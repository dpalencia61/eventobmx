import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BmxTableComponent } from './bmx-table.component';

describe('BmxTableComponent', () => {
  let component: BmxTableComponent;
  let fixture: ComponentFixture<BmxTableComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BmxTableComponent]
    });
    fixture = TestBed.createComponent(BmxTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
