import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BmxDetailComponent } from '../components/bmx-detail/bmx-detail.component';
import { HeaderComponent } from '../components/header/header.component';
import { PrincipalComponent } from '../principal/principal.component';
import { InfoComponent } from '../info/info.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: PrincipalComponent },
  {path: 'bmxDetail', component: BmxDetailComponent},
  {path: 'info', component: InfoComponent},
  {path: '**', pathMatch:'full',redirectTo:'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class NombreDelModuloRoutingModule { }
