import { Component, HostListener } from '@angular/core';
import { ScrollDispatcher } from '@angular/cdk/scrolling';
@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent {
  selectedImageIndex: number = 0;
  missionVisible = false;
  visionVisible = false;
  imagesVisible = false;


  constructor(private scrollDispatcher: ScrollDispatcher) { }


  @HostListener('window:scroll', ['$event'])
  checkScroll() {


    const missionElement = document.querySelector('.mission');
    const visionElement = document.querySelector('.vision');

    this.missionVisible = this.isElementInViewport(missionElement as HTMLElement);
    this.visionVisible = this.isElementInViewport(visionElement as HTMLElement);

    // Ocultar elementos cuando salen del viewport
    if (!this.missionVisible) {
      // Código para ocultar el elemento de la misión si es necesario
    }

    if (!this.visionVisible) {
      // Código para ocultar el elemento de la visión si es necesario
    }
  }

  private isElementInViewport(element: HTMLElement | null): boolean {
    if (!element) return false;
    const rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }
 
  
}
