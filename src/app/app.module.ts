import { NgModule,NO_ERRORS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { BmxDetailComponent } from './components/bmx-detail/bmx-detail.component';
import { BmxTableComponent } from './components/bmx-table/bmx-table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from 'src/sheared/material.module';
import { HttpClientModule } from '@angular/common/http';
import { InfoComponent } from './info/info.component';
import { NombreDelModuloRoutingModule } from './app-routing.module.ts/app-routing.module.ts.module';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { MatIconModule } from '@angular/material/icon';
import { MatSliderModule } from '@angular/material/slider';
import { CarouselComponent } from './carousel/carousel.component';
import { ModalContentComponent } from './modal-content/modal-content.component';
import { MatDialogModule } from '@angular/material/dialog'; // Asegúrate de importar MatDialogModule


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    BmxDetailComponent,
    BmxTableComponent,
    InfoComponent,
    CarouselComponent,
    ModalContentComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule,
    BrowserModule,
    RouterModule.forRoot([]),
    NombreDelModuloRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    MatIconModule,
    MatSliderModule,
    MatDialogModule 
  ],
  schemas: [
    NO_ERRORS_SCHEMA 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
